<?php

return [
    'credentials' => [
        'login' => env('SLETAT_LOGIN','admin@example.com'),
        'password' => env('SLETAT_PASS','secret')
    ],
    'default_params' => [
        //'s_showcase' => 1,
        //'cities' => null,
        'currencyAlias' => 'RUB',
        //'fake' => '',
        //'hotels' => '',
        //'groupBy' => 'hotel',
        //'hiddenOperators' => '',
        'includeDescriptions' => 1,
        'includeOilTaxesAndVisa' => 1,
        // 'meals' => '',
        // 'pageNumber' => '',
        'pageSize' => 500,
        's_nightsMax' => 3,
        's_nightsMin' => 15,
        //'stars' => '',
        'visibleOperators' => '3,4,6,7,9,14,19,20,38,54,112,213,302,304,380',
        'economOnly' => 0,
        'hideShopTours' => 1,
        's_hotelIsNotInStop'=>'true',
        's_hasTickets'=>'true',
        's_ticketsIncluded'=>'true',
        //'siteSessionId' => '',
        //'useAccount' => '',
        //'minHotelRating' => '',
        //'beachLines' => '',
    ],
    "citiesFrom" => [
        "Санкт-Петербург" => 1264,
        "Москва" => 832,
    ],
    'check_max_attempts' => 10,
    'actualized_attempts' => 2,
    'debug' => true,
    'pageSize' => 500,
];
