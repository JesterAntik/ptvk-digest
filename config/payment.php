<?php

return [
    'redirect_url' => env('APP_URL'),
    'paykeeper' => [
        'user' => env('PAYKEEPER_USER', 'user'),
        'password' => env('PAYKEEPER_PASSWORD', 'password'),
        'host' => env('PAYKEEPER_HOST', '/'),
        'secret' => env('PAYKEEPER_SECRET','secret'),
    ]
];
