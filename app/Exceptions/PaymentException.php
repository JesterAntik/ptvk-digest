<?php

namespace App\Exceptions;

class PaymentException extends \Exception
{
    /**
     * @var string
     */
    private $detail = '';
    /**
     * @var null
     */
    private $debugInfo;



    /**
     * PaymentException constructor.
     * @param string $message
     * @param string $detail
     * @param int $code
     * @param null $debugInfo
     */
    public function __construct($message = 'UnknownError', $detail = "", $code = 500, $debugInfo = null)
    {
        $this->debugInfo = $debugInfo;
        $this->detail = (empty($detail)) ? $message : $detail;
        parent::__construct($message, $code);
    }

    /**
     * @return string
     */
    public function getDetail()
    {
        return $this->detail;
    }

    /**
     * @return null
     */
    public function getDebugInfo()
    {
        return $this->debugInfo;
    }
}
