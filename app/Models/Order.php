<?php

namespace App\Models;

use Illuminate\Database\Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;

/**
 * Class Order
 *
 * @package App\Models\V2
 * @property int $id
 * @property string $status
 * @property double $amount
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $phone
 *
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Carbon|null $deleted_at
 * @property-read string $full_name
 * @property-read Collection|Payment[] $payments
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|Order onlyTrashed()
 * @method static bool|null restore()
 * @method static Builder|Order whereAmount($value)
 * @method static \Illuminate\Database\Query\Builder|Order withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Order withoutTrashed()
 */
class Order extends Model
{
    use SoftDeletes;
    /**
     * @var string
     */
    protected $table = "orders";

    /**
     * @var array
     */
    protected $fillable = [
        // 'id'
        'first_name',
        'last_name',
        'phone',
        'email',
        'amount',
        //'status',
    ];

    /**
     * @var array
     */
    protected $dates = [
        'deleted_at'
    ];

    /**
     * @var array
     */
    protected $visible = [
        'id',
        'first_name',
        'last_name',
        'phone',
        'email',
        'amount',
        'status',
        'full_name',
        'created_at',
    ];

    /**
     * @var array
     */
    protected $appends = [
        'full_name'
    ];


    /**
     * @return HasMany
     */
    public function payments()
    {
        return $this->hasMany(Payment::class, 'order_id', 'id');
    }

    /**
     * @return string
     */
    public function getFullNameAttribute()
    {
        return $this->first_name . " " . $this->last_name;
    }

    /**
     *
     */
    public function checkPaidStatus()
    {
        $paid_sum = $this->payments->where('status', 'paid')->sum('amount');
        if ($paid_sum >= $this->amount) {
            $this->status = "PAID";
            $this->save();
        }
    }


}
