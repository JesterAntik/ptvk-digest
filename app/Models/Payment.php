<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;

/**
 * Class Payment
 *
 * @package App\Models\V2
 * @property int $id
 * @property int|null $order_id
 * @property float $amount
 * @property string|null $invoice_id
 * @property string|null $url
 * @property string $status
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Carbon|null $deleted_at
 *
 * @method static Builder|Payment newModelQuery()
 * @method static Builder|Payment newQuery()
 * @method static Builder|Payment query()
 * @method static Builder|Payment whereAmount($value)
 * @method static Builder|Payment whereCreatedAt($value)
 * @method static Builder|Payment whereDeletedAt($value)
 * @method static Builder|Payment whereId($value)
 * @method static Builder|Payment whereInvoiceId($value)
 * @method static Builder|Payment whereOrderId($value)
 * @method static Builder|Payment whereStatus($value)
 * @method static Builder|Payment whereUpdatedAt($value)
 * @method static Builder|Payment whereUrl($value)
 */
class Payment extends Model
{
    use SoftDeletes;
    /**
     * @var string
     */
    protected $table='payments';

    /**
     * @var array
     */
    protected $fillable = [
        'amount',
        'order_id',
        'invoice_id',
        'payment_id',
        'url',
        'status',
    ];

    protected $casts = ['deleted_at'];

}
