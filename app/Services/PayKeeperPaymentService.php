<?php


namespace App\Services;


use App\Contracts\PaymentService;
use App\Exceptions\PaymentException;
use App\Models\Order;
use App\Models\Payment;
use Illuminate\Config\Repository;

/**
 * Class PayKeeperPaymentService
 * @package App\Services
 */
class PayKeeperPaymentService implements PaymentService
{
    /**
     * @var Repository
     */
    private $host;
    /**
     * @var string
     */
    private $auth;

    /**
     * PayKeeperPaymentService constructor.
     */
    public function __construct()
    {

        $this->host = config('payment.paykeeper.host');
        $this->auth = base64_encode(config('payment.paykeeper.user') . ':' . config('payment.paykeeper.password'));
    }

    /**
     * @param $method
     * @param $uri
     * @param $headers
     * @param bool $request
     * @return mixed
     */
    private function performAPIRequest($method, $uri, $headers, $request = false)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_URL, $this->host . $uri);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_HEADER, false);

        if ($request) {
            curl_setopt($curl, CURLOPT_POSTFIELDS, $request);
        }

        return json_decode(curl_exec($curl), true);
    }

    /**
     * @param $invoice_id
     * @return string
     */
    public function generateInvoiceUrl($invoice_id)
    {
        return 'https://' . $this->host . "/bill/$invoice_id/";
    }

    /**
     * @param $invoice_id
     * @return string
     * @throws PaymentException
     */
    public function checkInvoiceStatus($invoice_id)
    {
        $headers = Array();
        $headers[] = 'Content-Type: application/x-www-form-urlencoded';
        $headers[] = 'Authorization: Basic ' . $this->auth;
        $response = $this->performAPIRequest('GET', "/info/invoice/byid/?id=$invoice_id", $headers);
        if (isset($response['status'])) $status = $response['status'];
        else throw new PaymentException("Ошибка платежной системой. Попробуйте позже.",
            "Не удалось проверить статус платежа $invoice_id");
        return $status;
    }

    /**
     * @param Order $order
     * @throws PaymentException
     */
    public function checkPaymentsStatus(Order $order)
    {
        foreach ($order->payments as $payment) {
            $payment->status = $this->checkInvoiceStatus($payment->invoice_id);
            $payment->save();
        }
        $order->checkPaidStatus();
    }

    /**
     * @param Order $order
     * @return string
     * @throws PaymentException
     */
    public function init(Order $order)
    {
        $headers = Array();
        $headers[] = 'Content-Type: application/x-www-form-urlencoded';
        $headers[] = 'Authorization: Basic ' . $this->auth;
        $payment = new Payment([
            'order_id' => $order->id,
            'amount' => $order->amount,
        ]);

        $payment_data = array(
            "pay_amount" => $order->amount,
            "clientid" => $order->full_name,
            "orderid" => $order->id,
            "service_name" => "Оплата заказа №" . $order->id,
            "client_phone" => $order->phone,
        );

        $response = $this->performAPIRequest('GET', '/info/settings/token/', $headers);
        if (isset($response['token'])) $token = $response['token'];
        else throw new PaymentException("Ошибка платежной системой. Попробуйте позже.",
            "Не пришел token от Paykeeper");

        $request = http_build_query(array_merge($payment_data, array('token' => $token)));
        $response = $this->performAPIRequest('POST', '/change/invoice/preview/', $headers, $request);
        $payment->status = "created";

        if (isset($response['invoice_id'])) $payment->invoice_id = $response['invoice_id'];
        else throw new PaymentException("Не удалось сгенерировать счет на оплату. Попробуйте позже.",
            "Не пришел invoice_id от Paykeeper. Заказ #$order->id");
        $payment->status = "sent";

        $payment->url = $this->generateInvoiceUrl($payment->invoice_id);
        $payment->save();

        return $payment->url;

    }

    /**
     * @param array $data
     * @return mixed|void
     * @throws PaymentException
     */
    public function callback(array $data)
    {

        $params = collect($data)->only(['id', 'sum', 'clientid', 'orderid', 'key']);

        /** @var Order $order */
        $order = Order::find($params->get('orderid'));

        if (empty($order)) {
            throw new PaymentException('Unknown order', "Get payment to unknown order " . $params->get('orderid'), 500, $params->all());
        }

        //check correct received data
        if ($params->get('key', '') !=
            md5($params->get('id', '')
                . $params->get('sum', '')
                . $params->get('clientid', '')
                . $params->get('orderid', '')
                . config('payment.paykeeper.secret')
            )
        ) {
            throw new PaymentException('Incorrect key', "Hash not correct. Check secret.", 500, $params->all());
        }
        $this->checkPaymentsStatus($order);
        $hash = md5($params->get('id') . config('payment.paykeeper.secret'));
        return "OK $hash";
    }
}
