<?php

namespace App\Services;


use App\Contracts\SMSService;

class SMSCService implements SMSService
{

    private $url = 'https://smsc.ru/sys/send.php?';

    public function sendSMS($phoneNumber, $msg)
    {
        $params = [
            'login' => config('smsc.login'),
            'psw' => config('smsc.password'),
            'phones' => $phoneNumber,
            'mes' => $msg,
        ];
        $result_page = @file_get_contents($this->url . http_build_query($params));

        preg_match('/OK - (\d*) SMS, ID - (\d)/', $result_page, $success);
        preg_match('/ERROR = (\d*) (\(.*\))/', $result_page, $errors);
        $result =[];
        if (!empty($success[1])) {
            $result ['success'] = $success[1];
        }
        if (!empty($errors[1])) {
            $result ['error'] = $errors[1];
            $result ['error_message'] = $errors[2];
        }
        return $result;
    }
}
