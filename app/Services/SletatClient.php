<?php

namespace App\Services;


use Exception;
use GuzzleHttp\Client;
use Illuminate\Config\Repository;
use Illuminate\Support\Collection;
use Meng\AsyncSoap\Guzzle\Factory;
use Meng\AsyncSoap\SoapClientInterface;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use SoapHeader;
use stdClass;

/**
 * Class SletatRuClient
 * @package App\Sletat\Services
 */
class SletatClient
{

    /**
     *
     */
    const SLETAT_XMLGATE_URL = 'https://module.sletat.ru/XmlGate.svc?singleWSDL';
    /**
     *
     */
    const SLETAT_BASE_URI = 'https://module.sletat.ru/Main.svc/';
    /**
     *
     */
    const SLETAT_LOGGER_NAME = 'Sletat.Ru';
    /**
     *
     */
    const SLETAT_GET_TOURS_URI = 'GetTours?';
    /**
     *
     */
    const SLETAT_GET_LOAD_STATE_URI = 'GetLoadState?';
    /**
     *
     */
    const SLETAT_ACTUALIZE_PRICE_URI = 'ActualizePrice?';

    /**
     * @var Client
     */
    private $client;

    /**
     * @var array
     */
    private $credentials = [];

    /**
     * @var array
     */
    private $from = [];

    /**
     * @var array
     */
    private $params = [];

    /**
     * @var array
     */
    private $current_params = [];

    /**
     * @var Factory
     */
    private $soapFactory;

    /**
     * @var SoapClientInterface
     */
    private $soapClient;

    /**
     * @var Logger
     */
    private $logger;
    /**
     * @var Repository|mixed
     */
    private $debug;

    /**
     * SletatRuClient constructor.
     * @throws Exception
     */
    public function __construct()
    {
        $this->soapFactory = new Factory();

        $this->soapClient = $this->soapFactory->create(new Client(), self::SLETAT_XMLGATE_URL);

        $this->client = new Client([
            'base_uri' => self::SLETAT_BASE_URI,
        ]);
        $this->params = config('sletat.default_params');
        $this->credentials = config('sletat.credentials');
        $this->from = config('sletat.citiesFrom');

        /* TODO Use Sletat Exception and log in handler*/
        $this->logger = new Logger(self::SLETAT_LOGGER_NAME);
        $this->logger->pushHandler(new StreamHandler(storage_path('logs/sletat.log'), Logger::INFO));

        $this->debug = config('sletat.debug', false);
    }

    /**
     * @return array
     */
    public function getCredentials()
    {
        return $this->credentials;
    }

    /**
     * @param $credentials
     */
    public function setCredentials($credentials): void
    {
        $this->credentials = $credentials;
    }

    /**
     * @return array
     */
    public function getFrom(): array
    {
        return $this->from;
    }

    /**
     * @param array $from
     */
    public function setFrom(array $from): void
    {
        $this->from = $from;
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return $this->params;
    }

    /**
     * @param array $params
     */
    public function setParams(array $params): void
    {
        $this->params = $params;
    }

    /**
     * @return array
     */
    public function getCurrentParams(): array
    {
        return $this->current_params;
    }

    /**
     * @param array $current_params
     */
    public function setCurrentParams(array $current_params): void
    {
        $this->current_params = $current_params;
    }

    /**
     * @param $uri
     * @return mixed
     */
    private function sendJsonRequest($uri)
    {
        $httpResponse = $this->client->get($uri);
        if ($this->debug) {
            $this->logger->debug("Uri: $uri");
        }
        $response_content = $httpResponse->getBody()->getContents();
        $response = json_decode($response_content, true);

        foreach ($response as $result) {
            if ($result['IsError']) {
                $this->logger->error("$result[ErrorCode]: $result[ErrorMessage]", ['uri' => $uri, 'result' => $result]);
            }
            break;
        }
        return $response;
    }

    private function getSoapHeader()
    {
        return new SoapHeader("urn:SletatRu:DataTypes:AuthData:v1", "AuthInfo", [
            'Login' => $this->credentials['login'],
            'Password' => $this->credentials['password'],
        ]);
    }

    /**
     * @param string $name
     * @param array $params
     * @return array
     */
    public function getDictionary(string $name, $params = [])
    {
        $uri = "Get$name";
        if (!empty($params)) $uri .= "?" . http_build_query($params);
        $response = $this->client->get($uri);
        $list = json_decode($response->getBody()->getContents(), true)["Get" . $name . "Result"]['Data'];
        return $list;
    }


    /**
     * @param $Id
     * @return mixed
     */
    public function getHotelInfo($Id)
    {
        $info = $this->soapClient->call('GetHotelInformation', [['hotelId' => $Id]], [], [$this->getSoapHeader()]);

        return $info->GetHotelInformationResult;
    }

    /**
     * @param $Id
     * @return mixed|null
     */
    public function getHotelComments($Id)
    {
        $result = $this->soapClient->call('GetHotelComments', [['hotelId' => $Id]], [], [$this->getSoapHeader()]);
        if (!empty($result->GetHotelCommentsResult) && !empty($result->GetHotelCommentsResult->HotelComment))
            return $result->GetHotelCommentsResult->HotelComment;
        else return null;
    }


    /**
     * @param array $params
     * @return array
     */
    public function createRequest(array $params)
    {
        $this->current_params = array_merge($this->params, $this->credentials, $params);
        $uri = self::SLETAT_GET_TOURS_URI . http_build_query($this->current_params);
        $contents_array = $this->sendJsonRequest($uri);
        $requestId = array_get(array_get(array_get($contents_array, 'GetToursResult', []), 'Data', []), 'requestId', null);
        if (empty($requestId)) {
            $this->logger->error("No request Id", ['result' => array_get($contents_array, 'GetToursResult', [])]);
            return null;
        }

        return [
            'requestId' => $requestId,
            'parameters' => $this->current_params,
            'uri' => $uri,
        ];

    }

    /**
     * @param $requestId
     * @return bool
     */
    public function checkStates($requestId)
    {
        return $this->loadStates($requestId)->every(function ($state) {
            return $state['IsProcessed'] == true;
        });
    }

    /**
     * @param $requestId
     * @param $params
     * @return mixed
     */
    public function getRequestResult($requestId, $params)
    {
        $params = array_merge($this->params, $this->credentials, $params);
        $params['requestId'] = $requestId;
        $params['updateResult'] = 1;
        $uri = self::SLETAT_GET_TOURS_URI . http_build_query($params);
        $response = $this->sendJsonRequest($uri);
        return array_get(array_get($response, 'GetToursResult', []), 'Data', null);
    }


    /**
     * @param array $params
     * @return array|null
     */
    public function actualizeTourJson($params)
    {
        $uri = self::SLETAT_ACTUALIZE_PRICE_URI . http_build_query([
                'sourceId' => $params['sourceId'],
                'offerId' => $params['offerId'],
                'requestId' => $params['requestId'],
            ]);
        $response = $this->sendJsonRequest($uri);
        return array_get(array_get($response, 'ActualizePriceResult', []), 'Data', null);
    }

    /**
     * @param array $parameters
     * @return stdClass
     */
    public function actualizeTourXML($parameters)
    {
        $info = $this->soapClient->call('ActualizePrice', [$parameters], [], [$this->getSoapHeader()]);

        $result = $info->ActualizePriceResult;
        if ($result->IsError) {
            $type = 'soap';
            $this->logger->error("$result->ErrorMessage", compact('type', 'parameters', 'result'));
        }

        return $result;
    }

    /**
     * @param $requestId
     * @return Collection
     */
    public function loadStates($requestId)
    {
        $uri = self::SLETAT_GET_LOAD_STATE_URI . http_build_query(compact('requestId'));
        $response_data = $this->sendJsonRequest($uri);

        return array_get(array_get($response_data, 'GetLoadStateResult', []), 'Data', null);
    }

}
