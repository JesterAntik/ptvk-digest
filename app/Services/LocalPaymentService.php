<?php


namespace App\Services;


use App\Models\Order;
use App\Contracts\PaymentService;

/**
 * Class LocalPaymentService
 * @package App\Services
 */
class LocalPaymentService implements PaymentService
{

    /**
     * @param Order $order
     * @return string
     */
    public function init(Order $order)
    {
             $order->status = "PAID";
             $order->save();
             return config('payment.redirect_url');
    }

    /**
     * @param array $data
     * @return mixed|void
     */
    public function callback(array $data)
    {

    }
}
