<?php


namespace App\Contracts;


use App\Models\Order;

/**
 * Interface PaymentService
 * @package App\Contracts
 */
interface PaymentService
{
    /**
     * @param Order $order
     * @return string
     */
    public function init(Order $order);

    /**
     * @param array $data
     * @return mixed
     */
    public function callback(array $data);
}
