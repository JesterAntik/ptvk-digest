<?php

namespace App\Contracts;


interface SMSService
{
    public function sendSMS($phoneNumber, $msg);
}
