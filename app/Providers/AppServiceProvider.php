<?php

namespace App\Providers;

use App\Contracts\PaymentService;
use App\Contracts\SMSService;
use App\Services\LocalPaymentService;
use App\Services\PayKeeperPaymentService;
use App\Services\SMSCService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() == 'PRODUCTION') {
            $this->app->bind(PaymentService::class,PayKeeperPaymentService::class);
        } else {
            $this->app->bind(PaymentService::class,LocalPaymentService::class);
        }


        $this->app->bind(SMSService::class,SMSCService::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
